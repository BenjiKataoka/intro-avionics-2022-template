# Intro to Avionics Fall 2022 Software Template
This repository is a template repository for both the software and hardware groups for creating your own sensor and system code.

## Making a copy (fork)
To make a copy of this repository (so that you can make your own changes with your group), go to Gitlab and click on the Fork button.
This will make a new version of the repository on your account so that your group can use it.

## Code layout
The majority of the software group's code will go into `intro-avionics-2022-template.ino`. Note that you will have to change the name of the `.ino` file to match the name of your folder (because Arduino requires it.) 

The hardware group's code will primarily go in `hardware_manager.cpp` and potentially other files specific to your sensors/other breakouts, depending on how you structure your code.
